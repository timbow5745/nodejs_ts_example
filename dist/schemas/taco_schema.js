"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var joi_1 = __importDefault(require("@hapi/joi"));
exports.default = {
    name: joi_1.default.string().max(255),
    tortilla: joi_1.default.string(),
    toppings: joi_1.default.string(),
    vegetarian: joi_1.default.boolean(),
    soft: joi_1.default.boolean()
};
//# sourceMappingURL=taco_schema.js.map