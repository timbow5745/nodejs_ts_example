"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var routes_1 = __importDefault(require("./routes"));
var port = 8080, app = express_1.default();
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded());
routes_1.default(app);
app.listen(port, function () { return console.log("listening on " + port); });
//# sourceMappingURL=index.js.map