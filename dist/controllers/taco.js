"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var taco_schema_1 = __importDefault(require("../schemas/taco_schema"));
var joi_1 = __importDefault(require("@hapi/joi"));
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var dbPath = path_1.default.resolve(__dirname, '..', 'db.json');
console.log(dbPath);
var get_taco_list = function () {
    var tacos_json = fs_1.default.readFileSync(dbPath, 'utf-8');
    return JSON.parse(tacos_json).tacos;
};
function updateTaco(tacoData) {
    var tacos = get_taco_list();
    var schema = joi_1.default.object(taco_schema_1.default);
    var validatedTacoData = schema.validate(tacoData);
    if (!validatedTacoData.error) {
        var index = tacos.findIndex(function (taco) { return taco.name === tacoData.name; });
        if (index > -1) {
            (tacos[index] = tacoData);
            fs_1.default.writeFileSync(dbPath, JSON.stringify({ tacos: tacos }));
            return 'done';
        }
        else {
            return 'error';
        }
    }
    else {
        return 'error2';
    }
}
function addTaco(tacoData) {
    var tacos = get_taco_list();
    var schema = joi_1.default.object(taco_schema_1.default);
    var validatedTacoData = schema.validate(tacoData);
    if (!validatedTacoData.error) {
        tacos.push(tacoData);
        fs_1.default.writeFileSync(dbPath, JSON.stringify({ tacos: tacos }));
        return 'done';
    }
    else {
        return 'error';
    }
}
function removeTaco(name) {
    var tacos = get_taco_list();
    var index = tacos.findIndex(function (taco) { return taco.name === name; });
    if (index > -1) {
        tacos.splice(index, 1);
        fs_1.default.writeFileSync(dbPath, JSON.stringify({ tacos: tacos }));
        return 'done';
    }
    else {
        return 'error';
    }
}
function getTaco(name) {
    var tacos = get_taco_list();
    var taco = tacos.find(function (taco) { return taco.name === name; });
    return taco ? taco : 'not found';
}
function getTacos() {
    var tacos = get_taco_list();
    return tacos;
}
exports.default = { getTaco: getTaco, getTacos: getTacos, updateTaco: updateTaco, removeTaco: removeTaco, addTaco: addTaco };
//# sourceMappingURL=taco.js.map