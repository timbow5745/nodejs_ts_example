"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var algorithm_1 = __importDefault(require("../algorithm/algorithm"));
// const text = fs.readFileSync(path.resolve(__dirname, '..', 'algorithm/textSample.txt'), 'utf-8');
exports.default = (function (app) {
    app.post('/api/algorithm', function (req, res) {
        var results = algorithm_1.default(req.body.text);
        res.json(results);
    });
});
//# sourceMappingURL=algorithm_api.js.map