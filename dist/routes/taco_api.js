"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var taco_1 = __importDefault(require("../controllers/taco"));
exports.default = (function (app) {
    app.get('/api/tacos', function (req, res) {
        var tacos = taco_1.default.getTacos();
        res.json(tacos);
    });
    app.get('/api/taco/:name', function (req, res) {
        var taco = taco_1.default.getTaco(req.params.name);
        res.json(taco);
    });
    app.put('/api/taco/:name', function (req, res) {
        var result = taco_1.default.updateTaco(req.body);
        res.json(result);
    });
    app.delete('/api/taco/:name', function (req, res) {
        var result = taco_1.default.removeTaco(req.params.name);
        res.json(result);
    });
});
//# sourceMappingURL=taco_api.js.map