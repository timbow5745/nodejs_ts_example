"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var taco_api_1 = __importDefault(require("./taco_api"));
var algorithm_api_1 = __importDefault(require("./algorithm_api"));
exports.default = (function (app) {
    taco_api_1.default(app);
    algorithm_api_1.default(app);
});
//# sourceMappingURL=index.js.map