"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var escapeRegExp = function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
};
var replaceAll = function (s, find, replace) {
    return s.replace(new RegExp(escapeRegExp(find), "g"), replace);
};
var strip_text = function (text) {
    return text.replace(/[^a-z \-\']/gi, ' ');
};
var s = "RSTLNAEIOU-'".toLowerCase().split('').reduce(function (current, item) {
    current[item] = true;
    return current;
}, {});
var word_is_good = function (word) {
    var letters = word.split('');
    var bad_letters = {};
    var is_bad = false;
    letters.some(function (letter) {
        if (!s[letter]) {
            if (bad_letters[letter] > 0) {
                is_bad = true;
                return false;
            }
            bad_letters[letter] = 1;
        }
    });
    return is_bad ? false : word;
};
var fix_text = function (text) {
    var stripped_text = strip_text(text.toLowerCase());
    var array_text = stripped_text.split(' ').filter(function (word) { return word.length > 0; });
    // console.log(array_text);
    var filtered_text = array_text.filter(word_is_good);
    // console.log(filtered_text);
    var dups = filtered_text.reduce(function (acum, cur) {
        var _a;
        return Object.assign(acum, (_a = {}, _a[cur] = (acum[cur] | 0) + 1, _a));
    }, {});
    var sorted_dubs = JSON.parse(JSON.stringify(Object.keys(dups).map(function (key) {
        return { word: key, numberOfUses: dups[key] };
    })));
    sorted_dubs.sort(function (a, b) {
        return a.numberOfUses > b.numberOfUses ? -1 : a.numberOfUses < b.numberOfUses ? 1 : 0;
    });
    // console.log(sorted_dubs);
    var dup_winner = sorted_dubs[0];
    var no_dups_text = Array.from(new Set(filtered_text));
    var sorted_text = JSON.parse(JSON.stringify(no_dups_text));
    sorted_text.sort(function (a, b) {
        return a.length > b.length ? 1 : a.length < b.length ? -1 : 0;
    });
    // console.log(sorted_text);
    var response = { array: sorted_text, mostCommonWord: dup_winner };
    fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '..', 'textSampleResults.json'), JSON.stringify(response));
    return response;
};
exports.default = fix_text;
//# sourceMappingURL=algorithm.js.map