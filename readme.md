### Notes
The dist folder is already there with the first compile. I would need webpack or similar to copy files to dist like db.json, etc...

### Start
1) npm install
2) npm run build
3) npm run server
4) go to http://localhost:8080/api/tacos - Other routes found in src/routes

### Test
1) npm run test