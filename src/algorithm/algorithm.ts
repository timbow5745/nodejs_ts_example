
import path from "path";
import fs from 'fs';
const escapeRegExp = function escapeRegExp(str: string) {
  return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
};
const replaceAll = function (s: string, find: string, replace: string) {
  return s.replace(new RegExp(escapeRegExp(find), "g"), replace);
}; 
const strip_text = (text: string) => {
  return text.replace(/[^a-z \-\']/gi, ' ');
}
const s = "RSTLNAEIOU-'".toLowerCase().split('').reduce( (current, item) => {
  current[item] = true;
  return current;
}, {});
const word_is_good = (word: string) => {
      const letters = word.split('');
      const bad_letters = {};
      let is_bad = false; 
      letters.some((letter: string) => {
          if(!s[letter]){
              if(bad_letters[letter] > 0){
                  is_bad = true;
                  return false;
              }
              bad_letters[letter] = 1
          }
      });
      return is_bad ? false : word;
}

const fix_text = (text: string) => {
    const stripped_text = strip_text(text.toLowerCase());
    const array_text = stripped_text.split(' ').filter((word) => word.length > 0);
    // console.log(array_text);
    const filtered_text = array_text.filter(word_is_good)
    // console.log(filtered_text);
    const dups = filtered_text.reduce((acum,cur) => Object.assign(acum,{[cur]: (acum[cur] | 0)+1}),{});
    const sorted_dubs: Array<{word: string, numberOfUses: number}> = JSON.parse(JSON.stringify(Object.keys(dups).map((key) => {
            return {word: key, numberOfUses: dups[key]}
        })
    ));    
    sorted_dubs.sort((a, b) => {
        return a.numberOfUses > b.numberOfUses ? -1 : a.numberOfUses < b.numberOfUses ? 1 : 0;
    });
    // console.log(sorted_dubs);
    const dup_winner = sorted_dubs[0];
    const no_dups_text = Array.from(new Set(filtered_text));
    const sorted_text: Array<string> = JSON.parse(JSON.stringify(no_dups_text));    
    sorted_text.sort((a, b) => {
        return a.length > b.length ? 1 : a.length < b.length ? -1 : 0;
    });
    // console.log(sorted_text);
    
    const response = {array: sorted_text, mostCommonWord: dup_winner}
    fs.writeFileSync(path.resolve(__dirname, '..', 'textSampleResults.json'), JSON.stringify(response));
    return response;
}

export default fix_text;