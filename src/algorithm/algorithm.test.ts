import fix_text from './algorithm';
import path from 'path';
import fs from 'fs';

const text = fs.readFileSync(path.resolve(__dirname, '..', 'algorithm/textSample.txt'), 'utf-8');

describe('fix_text', () => {
    it('fix_text to return value', () => {
        const return_value = fix_text(text);
        console.log(return_value.array.length);
        expect(return_value.array.length).toEqual(72);     
    });
});