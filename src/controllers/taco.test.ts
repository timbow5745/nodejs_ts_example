import taco from './taco';


describe('taco update', () => {
    it('taco.updateTaco with invalid data to return error', () => {
        const return_value = taco.updateTaco({"name": "a",
        "tortilla": "corn",
        "toppings": "chorizo",
        "vegetarian": false,
        "soft": true});
        expect(return_value).toEqual('error');     
    });
    it('taco.updateTaco with valid data to return done', () => {
        const return_value = taco.updateTaco({
            "name": "chorizo taco",
            "tortilla": "corn",
            "toppings": "chorizo",
            "vegetarian": false,
            "soft": true
          });
        expect(return_value).toEqual('done');     
    });
})

describe('taco remove', () => {
    it('taco.removeTaco with invalid data to return error', () => {
        const return_value = taco.removeTaco("");
        expect(return_value).toEqual('error');     
    });
    it('taco.removeTaco with valid data to return done', () => {
        const taco_to_restore = taco.getTaco("chorizo taco");
        const return_value = taco.removeTaco("chorizo taco");
        if(typeof taco_to_restore !== 'string'){
            taco.addTaco(taco_to_restore);
        }
        expect(return_value).toEqual('done');     
    });
})