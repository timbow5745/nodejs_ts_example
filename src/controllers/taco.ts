import taco_schema, { taco } from "../schemas/taco_schema";
import Joi from '@hapi/joi';
import path from "path";
import fs from 'fs';
const dbPath = path.resolve(__dirname, '..', 'db.json');
console.log(dbPath);
const get_taco_list = (): Array<taco> => {
    const tacos_json = fs.readFileSync(dbPath, 'utf-8');
    return JSON.parse(tacos_json).tacos;
}
  function updateTaco (tacoData: taco): string {
    const tacos = get_taco_list();
    const schema = Joi.object(taco_schema);
    const validatedTacoData = schema.validate(tacoData);
    if(!validatedTacoData.error){
        const index = tacos.findIndex((taco) => taco.name === tacoData.name)
        if(index  > -1){
          (tacos[index] = tacoData);
          fs.writeFileSync(dbPath, JSON.stringify({tacos}));
          return 'done';
        }else{
          return 'error';
        }
    }else{
        return 'error2';
    }
  }
  function addTaco (tacoData: taco): string {
    const tacos = get_taco_list();
    const schema = Joi.object(taco_schema);
    const validatedTacoData = schema.validate(tacoData);
    if(!validatedTacoData.error){
        tacos.push(tacoData);
        fs.writeFileSync(dbPath, JSON.stringify({tacos}));
        return 'done';
    }else{
        return 'error';
    }
  }

  function removeTaco(name: string): string {
    let tacos = get_taco_list();
    const index = tacos.findIndex((taco) => taco.name === name)
    if(index > -1){
      tacos.splice(index, 1);
      fs.writeFileSync(dbPath, JSON.stringify({tacos}));
      return 'done';
    }else{
      return 'error';
    }
  }

  function getTaco(name: string): taco | string {
    const tacos = get_taco_list();
    const taco = tacos.find((taco) => taco.name === name);
    return taco ? taco : 'not found';
  }

  function getTacos(): Array<taco> {
    let tacos = get_taco_list();
    return tacos;
  }

  export default {getTaco, getTacos, updateTaco, removeTaco, addTaco}