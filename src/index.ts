import express from 'express';
import bodyParser from 'body-parser';
import routes from './routes';
const port = 8080,
app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
routes(app);

app.listen(port, () => console.log(`listening on ${port}`));