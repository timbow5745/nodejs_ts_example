import Joi from '@hapi/joi';
export default  {
    name: Joi.string().max(255),
    tortilla: Joi.string(),
    toppings: Joi.string(),
    vegetarian: Joi.boolean(),
    soft: Joi.boolean()
  }
  export interface taco {
      name: string,
      tortilla: string,
      toppings: string,
      vegetarian: boolean,
      soft: boolean
    }