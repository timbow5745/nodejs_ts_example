import taco_api from './taco_api';
import algorithm_api from './algorithm_api';
import * as core from "express-serve-static-core";

export default  (app: core.Express) => {
    taco_api(app);
    algorithm_api(app);
};