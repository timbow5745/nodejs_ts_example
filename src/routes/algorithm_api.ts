
import algorithm from '../algorithm/algorithm'
import * as core from "express-serve-static-core";
import path from 'path';
import fs from 'fs';
// const text = fs.readFileSync(path.resolve(__dirname, '..', 'algorithm/textSample.txt'), 'utf-8');

export default (app: core.Express) => {
    app.post('/api/algorithm', (req, res) => {
        const results = algorithm(req.body.text);
        res.json(results);
      });

}