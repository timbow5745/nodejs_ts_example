

const {defaults} = require('jest-config');
module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    globals: {
      "ts-jest": {
        tsConfig: "tsconfig.jest.json"
      }
    }, 
    moduleFileExtensions: [...defaults.moduleFileExtensions, "js", "json", "jsx", "ts", "tsx", "node"],
    
  }